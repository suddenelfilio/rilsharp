﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net.SuddenElfilio.RilSharp
{
    public enum ReadState
    {
        Unread = 0,
        Read = 1,
        All = 2
    }
}
