﻿using System;

namespace Net.SuddenElfilio.RilSharp
{
    public class Limits
    {

        public int UserLimit { get; set; }
        public int UserLimitRemaining { get; set; }
        public TimeSpan UserLimitReset { get; set; }

        public int ApiLimit { get; set; }
        public int ApiLimitRemaining { get; set; }
        public TimeSpan ApiLimitReset { get; set; }
    }
}
