﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Net.SuddenElfilio.RilSharp
{
    [DataContract]
    public class RilListItem
    {
        [DataMember(Name = "item_id")]
        public int ID { get; set; }

        [DataMember(Name = "url")]
        public string Url { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "time_updated")]
        public double UpdatedOn { get; set; }

        [DataMember(Name = "time_added")]
        public double AddedOn { get; set; }

        [DataMember(Name = "tags")]
        public string Tags { get; set; }

        [DataMember(Name = "state")]
        public ReadState State { get; set; }

    }
}
