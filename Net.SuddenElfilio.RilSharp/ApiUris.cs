﻿namespace Net.SuddenElfilio.RilSharp
{
    internal sealed class ApiUris
    {
        public const string SignUp = "https://readitlaterlist.com/v2/signup?username={0}&password={1}&apikey={2}";
        public const string Authenticate = "https://readitlaterlist.com/v2/auth?username={0}&password={1}&apikey={2}";
        public const string Api = "https://readitlaterlist.com/v2/api?apikey={0}";
        public const string Add = "https://readitlaterlist.com/v2/add?username={0}&password={1}&apikey={2}&url={3}&title={4}";
        public const string Stats = "https://readitlaterlist.com/v2/stats?username={0}&password={1}&apikey={2}&format=json";
        public const string Get = "https://readitlaterlist.com/v2/get?username={0}&password={1}&apikey={2}&format=json&state={3}&myAppOnly={4}&since={5}&count={6}&page={7}&tags={8}";
        public const string Send = "https://readitlaterlist.com/v2/send?username={0}&password={1}&apikey={2}";

    }
}
