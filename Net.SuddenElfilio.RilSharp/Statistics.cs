﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Net.SuddenElfilio.RilSharp
{
    [DataContract]
    public class Statistics
    {
        [DataMember(Name = "user_since")]
        public long UserSince { get; set; }
        [DataMember(Name = "count_list")]
        public long ListCount { get; set; }
        [DataMember(Name = "count_unread")]
        public long UnreadCount { get; set; }
        [DataMember(Name = "count_read")]
        public long ReadCount { get; set; }

        public DateTime UserSinceDateTime
        {
            get
            {
                return UnixTime.ToDateTime(UserSince);
            }
        }
    }
}
