﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net.SuddenElfilio.RilSharp
{
    internal sealed class UnixTime
    {

        internal static DateTime ToDateTime(double seconds)
        {
            return new DateTime(1970,1,1,0,0,0).AddSeconds(seconds);
        }

        internal static double FromDatetime(DateTime date)
        {
            return (date - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds;
        }
    }
}
