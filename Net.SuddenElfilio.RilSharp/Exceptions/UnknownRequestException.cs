﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net.SuddenElfilio.RilSharp.Exceptions
{
    public class UnknownRequestException : Exception
    {
        public UnknownRequestException(string message):base(string.Format("An exception occured while performing an Api request: {0}",message))
        {
            
        }
    }
}
