﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net.SuddenElfilio.RilSharp.Exceptions
{
    public class LimitExceededException : Exception
    {
        public Limits Limits { get; set; }

        public LimitExceededException(Limits limit)
        {
            Limits = limit;
        }

        public override string Message
        {
            get
            {
                return "An API limit is exceeded for this hour.";
            }
        }
    }
}
