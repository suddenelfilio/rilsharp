﻿using System;

namespace Net.SuddenElfilio.RilSharp.Exceptions
{
    public class UsernameAlreadyTakenException : Exception
    {
        public string UserName { get; set; }

        public UsernameAlreadyTakenException(string userName)
        {
            UserName = userName;
        }

        public override string Message
        {
            get
            {
                return string.Format("The username {0} is already taken. Registration Failed.", UserName);
            }
        }
    }
}
