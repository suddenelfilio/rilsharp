﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net.SuddenElfilio.RilSharp.Exceptions
{
    public class InvalidApiRequestException: Exception
    {
        public InvalidApiRequestException(string message): base(string.Format("Invalid API Request: {0}",message))
        {
            
        }
    }
}
