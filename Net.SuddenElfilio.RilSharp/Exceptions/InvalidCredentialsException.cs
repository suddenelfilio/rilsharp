﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net.SuddenElfilio.RilSharp.Exceptions
{
    public class InvalidCredentialsException : Exception
    {
        public override string Message
        {
            get
            {
                return "Api request failed. Invalid credentials.";
            }
        }
    }
}
