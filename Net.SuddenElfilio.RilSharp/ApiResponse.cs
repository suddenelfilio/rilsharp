﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net.SuddenElfilio.RilSharp
{
    public class ApiResponse
    {
        public int Status { get; set; }
        
        public string Error { get; set; }

        public Limits LimitStatus { get; set; }

        public string Response { get; set; }
    }
}
