﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Net.SuddenElfilio.RilSharp
{
    public enum SendType
    {
        New = 0,
        Read = 1,
        Update_title = 2,
        Update_tags = 3
    }
}
