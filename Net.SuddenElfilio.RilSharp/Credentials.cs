﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Net.SuddenElfilio.RilSharp
{
    public class Credentials
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ApiKey { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(UserName))
                throw new ArgumentNullException("UserName", "The UserName property cannot be null or empty.");
            if (string.IsNullOrEmpty(Password))
                throw new ArgumentNullException("Password", "The Password property cannot be null or empty.");
            if (string.IsNullOrEmpty(ApiKey))
                throw new ArgumentNullException("ApiKey", "The ApiKey property cannot be null or empty.");
         

        }
    }
}
