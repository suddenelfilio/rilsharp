﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using System;

namespace Net.SuddenElfilio.RilSharp
{
    [DataContract]
    public class RilList
    {
        [DataMember(Name = "status")]
        public ListStatus Status { get; set; }

        [DataMember(Name = "since")]
        public double Since { get; set; }

        [DataMember(Name = "list")]
        public Dictionary<string, RilListItem> Items { get; set; }

        public DateTime SinceDate
        {
            get
            {
                return UnixTime.ToDateTime(Since);
            }
        }
    }
}
