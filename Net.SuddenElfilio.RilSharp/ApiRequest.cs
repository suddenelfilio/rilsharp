﻿using System;
using System.Net;
using Net.SuddenElfilio.RilSharp.Exceptions;
using System.Collections.Generic;

namespace Net.SuddenElfilio.RilSharp
{
    internal class ApiRequest
    {
        internal ApiRequest(string url)
        {
            Url = url;
        }

        internal string Url { get; set; }

        internal ApiResponse Execute(params string[] arguments)
        {
            var cl = new WebClient();
            try
            {
                Url = string.Format(Url, arguments);

                string result = cl.DownloadString(Url);
                var response = new ApiResponse
                                   {
                                       LimitStatus = GetLimits(cl),
                                       Status = 200,
                                       Response = result
                                   };
                return response;
            }
            catch (WebException wex)
            {

                return new ApiResponse()
                {
                    LimitStatus = GetLimits(cl),
                    Status = (int)((HttpWebResponse)wex.Response).StatusCode,
                    Error = wex.Message
                };

            }
            catch (Exception ex)
            {
                throw new UnknownRequestException(ex.ToString());
            }
        }

        internal ApiResponse ExecutePost(SendType type, string body, params string[] arguments)
        {
            var cl = new WebClient();
            try
            {
                Url = string.Format(Url, arguments);
                cl.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                string result = cl.UploadString(Url, body);
                var response = new ApiResponse
                {
                    LimitStatus = GetLimits(cl),
                    Status = 200,
                    Response = result
                };
                return response;
            }
            catch (WebException wex)
            {

                return new ApiResponse()
                {
                    LimitStatus = GetLimits(cl),
                    Status = (int)((HttpWebResponse)wex.Response).StatusCode,
                    Error = wex.Message
                };

            }
            catch (Exception ex)
            {
                throw new UnknownRequestException(ex.ToString());
            }
        }

        private static Limits GetLimits(WebClient cl)
        {
            if (cl.ResponseHeaders != null)
                return new Limits
                           {
                               ApiLimit = int.Parse(cl.ResponseHeaders["X-Limit-Key-Limit"] ?? "0"),
                               ApiLimitRemaining = int.Parse(cl.ResponseHeaders["X-Limit-Key-Remaining"] ?? "0"),
                               ApiLimitReset = new TimeSpan(0, 0, 0, int.Parse(cl.ResponseHeaders["X-Limit-Key-Reset"] ?? "0")),
                               UserLimit = int.Parse(cl.ResponseHeaders["X-Limit-User-Limit"] ?? "0"),
                               UserLimitRemaining = int.Parse(cl.ResponseHeaders["X-Limit-User-Remaining"] ?? "0"),
                               UserLimitReset = new TimeSpan(0, 0, 0, int.Parse(cl.ResponseHeaders["X-Limit-User-Reset"] ?? "0"))
                           };

            return null;
        }
    }
}