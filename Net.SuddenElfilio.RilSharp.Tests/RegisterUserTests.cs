﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Net.SuddenElfilio.RilSharp.Exceptions;

namespace Net.SuddenElfilio.RilSharp.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class RegisterUserTests
    {
        [TestMethod]
        public void RegisterExistingUser()
        {
            try
            {
                new RilClient(new Credentials
                                  {
                                      UserName = TestConfig.username,
                                      Password = TestConfig.password,
                                      ApiKey = TestConfig.apikey
                                  }).RegisterClient();
                Assert.Fail();
            }
            catch (UsernameAlreadyTakenException)
            {
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void RegisterNewUser()
        {
            try
            {
                string user = string.Format("testuser{0}", new Random().Next(1, 11000));
                Assert.IsTrue(new RilClient(new Credentials
                                                {
                                                    UserName = TestConfig.username,
                                                    Password = TestConfig.password,
                                                    ApiKey = TestConfig.apikey
                                                }).RegisterClient());
                Debug.Write(user);
            }
            catch (UsernameAlreadyTakenException)
            {
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

   
    }
}