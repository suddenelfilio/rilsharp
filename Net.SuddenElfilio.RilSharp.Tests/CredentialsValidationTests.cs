﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Net.SuddenElfilio.RilSharp.Tests
{
    [TestClass]
    public class CredentialsValidationTests
    {
        [TestMethod]
        public void ValidateCredentialsInvalidUserNameNull()
        {
            try
            {
                Assert.IsTrue(new RilClient(new Credentials
                {
                    UserName = null,
                    Password = TestConfig.password,
                    ApiKey = TestConfig.apikey
                }).RegisterClient());
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.IsTrue(ex.Message.StartsWith("The UserName property cannot be null or empty."));
            }
        }

        [TestMethod]
        public void ValidateCredentialsInvalidUserNameEmpty()
        {
            try
            {
                Assert.IsTrue(new RilClient(new Credentials
                {
                    UserName = "",
                    Password = TestConfig.password,
                    ApiKey = TestConfig.apikey
                }).RegisterClient());
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.IsTrue(ex.Message.StartsWith("The UserName property cannot be null or empty."));
            }
        }

        [TestMethod]
        public void ValidateCredentialsInvalidPasswordNull()
        {
            try
            {
                Assert.IsTrue(new RilClient(new Credentials
                {
                    UserName = TestConfig.username,
                    Password = null,
                    ApiKey = TestConfig.apikey
                }).RegisterClient());
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.IsTrue(ex.Message.StartsWith("The Password property cannot be null or empty."));
            }
        }

        [TestMethod]
        public void ValidateCredentialsInvalidPasswordEmpty()
        {
            try
            {
                Assert.IsTrue(new RilClient(new Credentials
                {
                    UserName = TestConfig.username,
                    Password = "",
                    ApiKey = TestConfig.apikey
                }).RegisterClient());
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.IsTrue(ex.Message.StartsWith("The Password property cannot be null or empty."));
            }
        }

        [TestMethod]
        public void ValidateCredentialsInvalidApiKeyNull()
        {
            try
            {
                Assert.IsTrue(new RilClient(new Credentials
                {
                    UserName = TestConfig.username,
                    Password = TestConfig.password,
                    ApiKey =null
                }).RegisterClient());
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.IsTrue(ex.Message.StartsWith("The ApiKey property cannot be null or empty."));
            }
        }

        [TestMethod]
        public void ValidateCredentialsInvalidApiKeyEmpty()
        {
            try
            {
                Assert.IsTrue(new RilClient(new Credentials
                {
                    UserName = TestConfig.username,
                    Password = TestConfig.password,
                    ApiKey = ""
                }).RegisterClient());
                Assert.Fail();
            }
            catch (ArgumentNullException ex)
            {
                Assert.IsTrue(ex.Message.StartsWith("The ApiKey property cannot be null or empty."));
            }
        }
    }
}
