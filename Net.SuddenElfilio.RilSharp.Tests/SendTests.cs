﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Net.SuddenElfilio.RilSharp.Tests
{
    [TestClass]
    public class SendTests
    {
        [TestMethod]
        public void SendNewItemsTest()
        {
            var result = new RilClient(new Credentials
            {
                UserName = TestConfig.username,
                Password = TestConfig.password,
                ApiKey = TestConfig.apikey
            }).Send(SendType.New, new List<RilListItem> { new RilListItem { Title = "test1", Url = "http://www.google.be?1" }, new RilListItem { Title = "test2", Url = "http://www.google.be?2" } });

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void SendReadItemsTest()
        {
            var result = new RilClient(new Credentials
            {
                UserName = TestConfig.username,
                Password = TestConfig.password,
                ApiKey = TestConfig.apikey
            }).Send(SendType.Read, new List<RilListItem> { new RilListItem { Title = "test1", Url = "http://www.google.be?1" } });

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void SendUpdateTitleTest()
        {
            var result = new RilClient(new Credentials
            {
                UserName = TestConfig.username,
                Password = TestConfig.password,
                ApiKey = TestConfig.apikey
            }).Send(SendType.Update_title, new List<RilListItem> { new RilListItem { Title = "tester2", Url = "http://www.google.be?2" } });

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void SendUpdateTagsTest()
        {
            var result = new RilClient(new Credentials
            {
                UserName = TestConfig.username,
                Password = TestConfig.password,
                ApiKey = TestConfig.apikey
            }).Send(SendType.Update_tags, new List<RilListItem> { new RilListItem { Url = "http://www.google.be?2", Tags = "test,ok"} });

            Assert.IsTrue(result);
        }
    }
}
