﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Net.SuddenElfilio.RilSharp.Exceptions;

namespace Net.SuddenElfilio.RilSharp.Tests
{
    [TestClass]
    public class AuthenticateUserTests
    {
        [TestMethod]
        public void AuthenticateExistingUser()
        {
            try
            {
                Assert.IsTrue(new RilClient(new Credentials
                {
                    UserName = TestConfig.username,
                    Password = TestConfig.password,
                    ApiKey = TestConfig.apikey
                }).Authenticate());
              
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void AuthenticateNonExistingUser()
        {
            try
            {
                new RilClient(new Credentials
                {
                    UserName = TestConfig.username,
                    Password = TestConfig.password,
                    ApiKey = TestConfig.apikey
                }).Authenticate();
                Assert.Fail();
            }
            catch (InvalidCredentialsException ex1)
            {
                Assert.IsTrue(true);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}
