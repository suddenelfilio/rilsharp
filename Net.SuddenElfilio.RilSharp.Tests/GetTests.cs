﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Net.SuddenElfilio.RilSharp.Tests
{
    [TestClass]
    public class GetTests
    {
        [TestMethod]
        public void GetTest()
        {
            var result = new RilClient(new Credentials
            {
                UserName = TestConfig.username,
                Password = TestConfig.password,
                ApiKey = TestConfig.apikey
            }).Get(ReadState.All,null,null,null,false,false);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetMyAppOnlyTest()
        {
            var result = new RilClient(new Credentials
            {
                UserName = TestConfig.username,
                Password = TestConfig.password,
                ApiKey = TestConfig.apikey
            }).Get(ReadState.All, null, null, null, true, false);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetReadTest()
        {
            var result = new RilClient(new Credentials
            {
                UserName = TestConfig.username,
                Password = TestConfig.password,
                ApiKey = TestConfig.apikey
            }).Get(ReadState.Read, null, null, null, false, false);

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GetReadLimitCountTest()
        {
            var result = new RilClient(new Credentials
            {
                UserName = TestConfig.username,
                Password = TestConfig.password,
                ApiKey = TestConfig.apikey
            }).Get(ReadState.Read, null, 5, 1, false, false);

            Assert.IsNotNull(result);
        }
    }
}
