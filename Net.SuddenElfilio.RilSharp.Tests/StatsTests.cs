﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Net.SuddenElfilio.RilSharp.Tests
{
    [TestClass]
    public class StatsTests
    {
        [TestMethod]
        public void StatsTest()
        {
            var result = new RilClient(new Credentials
            {
                UserName = TestConfig.username,
                Password = TestConfig.password,
                ApiKey = TestConfig.apikey
            }).Stats();

            Assert.IsNotNull(result);
        }
    }
}
