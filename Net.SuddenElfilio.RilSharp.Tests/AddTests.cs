﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Net.SuddenElfilio.RilSharp.Tests
{
    [TestClass]
    public class AddTests
    {
        [TestMethod]
        public void AddUrlAutoTitleTest()
        {
            Assert.IsTrue(new RilClient(new Credentials
            {
                UserName = TestConfig.username,
                Password = TestConfig.password,
                ApiKey = TestConfig.apikey
            }).Add("http://www.google.be", "test", true));

        }

        [TestMethod]
        public void AddUrlTest()
        {
            Assert.IsTrue(new RilClient(new Credentials
            {
                UserName = TestConfig.username,
                Password = TestConfig.password,
                ApiKey = TestConfig.apikey
            }).Add("http://www.google.be", "MyGoogleTest", false));

        }
    }
}
